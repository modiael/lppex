# Lenguajes y Paradigmas de Programación
## Hoja de Problemas
### Tema 2

<!-- Escuela Superior de Ingeniería Informática
Lenguajes y Sistemas Informáticos
Universidad de La Laguna -->

1. ¿Qué se entiende por abstracción?:star:

    >La abstracción es el proceso de identificar diseños comunes que tienen variaciones sistemáticas; una abstracción representa el diseño común, y provee un medio para especificar la variación a usar.

2. ¿Qué estructura dan los traductores de lenguajes de programación de alto nivel a la memoria de ejecución?

    >Una pila de llamadas (pila de ejecución) , que está compuesto de stack frames (marcos de pila) , a veces llamados registros de activación. Es una estructura de pila de datos que guarda información sobre las subrutinas activas de un programa de ordenador y su función principal es guardar las direcciones de retorno de las subrutinas activas.

3. ¿Qué es un procedimiento (subprograma)?:star:

    >Es una secuencia de instrucciones de programa que realiza una determinada tarea, empaquetado en una unidad.

4. ¿Qué es un Registro de Activación (Stack Frame)?:star:

    >Es una estructura de datos dependiente de la máquina que contienen la información de estado de una subrutina. Cada stack frame corresponde a una llamada a una subrutina activa, es decir, que todavía no ha terminado con un retorno a quien la llamó.

5. ¿Cúales son las componentes de un registro de activación?:star:

    >Los componentes de un registro de activación son los valores de los parámetros pasados a la rutina, el espacio para las variables locales de la rutina, la dirección de retorno del que llama a la rutina, campos de control del registro de activación y el valor a devolver.

6. Dibuje el flujo de ejecución de un programa principal en el que se hace una llamada a un subprograma para sumar dos números. :sos:
    ```c++
    //Programa Principal
    int main(){
    int x = 5; int y = 2;
    int resultado;
      resultado = suma(x,y);
      cout >> x << " + " << y << " = " << resultado;
    }
    //Subprograma
    int suma(int x, int y){
      return x+y;
    } //Salida 5 + 2 = 7
    ```
    >Se comienza en el registro de activación del programa principal, donde se almacenan las variables globales x e y con los valores 5 y 2 respectivamente(son variables locales al main), además de la variable resultad (sin valor). A continuación, el programa principal realiza una llamada a la subrutina `suma`, por lo que se crea un registro de activación en la pila de llamadas para esa subrutina. En su registro de activación se guardan los valores de los párametros pasados a la misma (x e y) y la dirección de retorno al programa principal. De esta manera, se ejecuta el código de la subrutina `suma` y suma los dos números, de tal forma que se guarda el valor a retornar en el registro de activación de la subrutina. Una vez finalizada la subrutina, con la dirección de retorno se sigue ejecutando el programa principal desde donde se realizó la llamada a la subrutina y se muestra el resultado.

7. ¿Qué se entiende por programación procedural?

    >Es un paradigma de la programación que consiste en basarse de un número muy bajo de expresiones repetidas, englobarlas todas en un procedimiento o función y llamarlo cada vez que tenga que ejecutarse. Muchas veces es aplicable tanto en lenguajes de programación de bajo nivel como en lenguajes de alto nivel. En el caso de que esta técnica se aplique en lenguajes de alto nivel, recibirá el nombre de Programación funcional.

8. ¿Cúal es el nombre de la instrucción en la que se basaban los primeros lenguajes de programación para modificar la secuencia de ejecución de las instrucciones mediante una transferencia incondicional de su control?:star:

    >El nombre de la instrucción es go to.

9. ¿Por qué se considera perjudicial el uso de sentencias go to? (E. Dijkstra, “Letters to the Editor: Go to Statement Considered Harmful”, Comm. ACM vol. 11, n.3, 147-148, 1968.)

    >Se considera perjudicial debido a la dificultad que presenta para poder seguir adecuadamente el flujo del programa, tan necesario para verificar y corregir los programas. Esta sentencia podría conducir a "código espagueti", que es mucho más difícil de seguir y de mantener, y era la causa de muchos errores de programación.

10. ¿Qué establece el Teorema Fundamental de la Estructura? (C. Bo ̈hm, G. Jacopini, “Flow dia- grams, Turing Machines and Languages with only Two Formation Rules”, Comm. ACM vol. 9, n. 5, 366-371, 1966.):star:


    >El teorema del programa estructurado es un resultado en la teoría de lenguajes de programación. Establece que toda función computable puede ser implementada en un lenguaje de programación que combine sólo tres estructuras lógicas. Esas tres formas (también llamadas estructuras de control) específicamente son:
      * Secuencia: ejecución de una instrucción tras otra.
      * Selección: ejecución de una de dos instrucciones (o conjuntos), según el valor de una variable booleana.
      * Iteración: ejecución de una instrucción (o conjunto) mientras una variable booleana sea 'verdadera'. Esta estructura lógica también se conoce como ciclo o bucle.
    Este teorema demuestra que la instrucción GOTO no es estrictamente necesaria y que para todo programa que la utilice existe otro equivalente que no hace uso de dicha instrucción.

11. ¿Qué se entiende por programación estructurada o modular?:star:

  >  La programación estructurada es un paradigma de programación orientado a mejorar la claridad, calidad y tiempo de desarrollo de un programa de computadora, utilizando únicamente subrutinas y tres estructuras: secuencia, selección (if y switch) e iteración (bucles for y while), considerando innecesario y contraproducente el uso de la instrucción de transferencia incondicional (GOTO), que podría conducir a "código espagueti", que es mucho más difícil de seguir y de mantener, y era la causa de muchos errores de programación.

12. ¿Cúales son las principales características de la programación estructurada?
  >  * Los programas son más fáciles de entender, pueden ser leídos de forma secuencial y no hay necesidad de hacer engorrosos seguimientos en saltos de líneas (GOTO) dentro de los bloques de código para intentar entender la lógica.
    * La estructura de los programas es clara, puesto que las instrucciones están más ligadas o relacionadas entre sí.
    * Reducción del esfuerzo en las pruebas y depuración. El seguimiento de los fallos o errores del programa (debugging) se facilita debido a su estructura más sencilla y comprensible, por lo que los errores se pueden detectar y corregir más fácilmente.
    * Reducción de los costos de mantenimiento. Análogamente a la depuración, durante la fase de mantenimiento, modificar o extender los programas resulta más fácil.
    * Los programas son más sencillos y más rápidos de confeccionar.
    * Se incrementa el rendimiento de los programadores.

13. ¿Qué es Ruby?:star:

    >Es un lenguaje de programación interpretado, reflexivo y orientado a objetos, creado por el programador japonés Yukihiro "Matz" Matsumoto.

14. ¿Cúal es la principal característica de Ruby?:star:

    >La principal característica es que es un lenguaje orientado a objetos auténtico. Todo lo que se manipula en Ruby es un objeto, y los resultados de esas manipulaciones son también objetos.

15. ¿Cúal es el constructor estándar?:star:

    >El constructor estándar se llama new.

16. ¿Cómo se define un método en Ruby?:star:

    >Un método se define con la palabra clave def, seguido del nombre del método y de sus parámetros entre paréntesis(los paréntesis son opcionales).

17. ¿Cúal es el separador de sentencias en Ruby?:star:

    >El separador de sentencias es el salto de línea. Cada línea de código es una sentencia.

    ```ruby
    def nombre-método(lista_argumentos)
        ...
    end

    ```

18. ¿Cómo se especifican comentarios en Ruby?:star:

    >Los comentarios de una sola línea comienzan con el carácter # .

    >Los comentarios de múltiples líneas comienzan con `=begin` y se termina con `=end`.


19. ¿Cómo se especifican la cadenas literales en Ruby?

    >Se especifican con comillas simples o con comillas dobles. Una cadena incluida entre comillas simples se convierte en el valor de la cadena. Ejemplo: 'hola\n' → hola\n

20. ¿Qué se entiende por interpolación de expresiones? ¿Cúal es su sintaxis? ¿Cúal es su semántica?

    >Es el proceso de insertar el resultado de una expresión dentro de un String. La forma de hacerlo es `#{​ expresión }`​. Para poder realizar esta interpolación, es necesario que la expresión esté en un string que está incluido entre comillas dobles.

21. ¿Qué indican los siguientes prefijos en la declaración de una variable?:star:
    * `$` Variables globales
    * `@` Variables de instancia
    * `@@` Variables de clase

22. ¿Cómo se definen los identificadores en Ruby?

    >Los identificadores consisten en letras, dígitos decimales y el carácter guión bajo. Empiezan con una letra (incluyendo el guión bajo). No hay restricciones en cuanto a la longitud de los identificadores en Ruby.

23. ¿Qué es un array? ¿Cúal es la sintaxis para definirlos en Ruby?:star:

    >Un array​ (o lista) es un conjunto ordenado: cada posición en la lista es una variable que podemos leer y/o escribir. Los elementos son accedidos a través de un entero.
    La sintaxis se puede representar de varias formas:
    `[ 'ant', 'bee', 'cat', 'dog', 'elk' ]`
    `%w{ ant bee cat dog elk }`
    `Array.new`

24. ¿Qué es un hash? ¿Cúal es la sintaxis para definirlos en Ruby? :star:

    >Este objeto es de gran utilidad para crear arreglos con una clave. Este objeto consiste en una lista de pares Clave/Valor separados por coma (,) entre secuencias y (=>) entre la clave y el valor.
    La sintaxis se puede representar de varias formas:
    ```ruby
    -inst_section = {
      'cello'=> 'string',
      'clarinet' => 'woodwind',
      'drum'=> 'percussion',
      'oboe'=> 'woodwind',
      'trumpet'=> 'brass',
      'violin'=> 'string'
    }
    -inst_section = {
      :cello=> 'string',
      :clarinet => 'woodwind',
      :drum=> 'percussion', #CON SÍMBOLOS
      :oboe=> 'woodwind',
      :trumpet=> 'brass',
      :violin=> 'string'
    }
    -inst_section = {
      cello: 'string',
      clarinet: 'woodwind',
      drum: 'percussion',
      oboe: 'woodwind',
      trumpet: 'brass',
      violin: 'string'
    }
    ```

25. ¿Qué es nil en Ruby?:star:

    >Es un objeto que representa “nada”.

26. ¿Qué es un symbol? ¿Cúal es la sintaxis para definirlos en Ruby?

    >Un symbol es un nombre constante que no tienes que predeclarar y se garantiza que es único. Un symbol empieza con dos puntos (:) y normalmente es seguido por algún tipo de nombre.

27. ¿Qué números son 010, 0x1F, 0b1111?:star:

    >010 -> 2 en decimal.

    >0x1F -> 31 en decimal.

    >0b1111 -> 15 en decimal.

28. ¿Es correcto escribir subguiones en un número (por ejemplo: 1_000_000)?:star:

    >Es correcto y se utiliza para separar dígitos para mejorar la legibilidad del número (son ignorados por Ruby), excepto poner subguiones al principio o al final del número.

29. ¿Cúal es la sintaxis para definir sentencias condicionales en Ruby?:star:

    ```ruby
    if (condition) statement
    elsif (condition) statement
    elsif (condition) statement
    ...
    else (condition) statement
    end
    ```
    ```ruby
    case expression
      [when expression [, expression ...] [then]
      code ]...
      [else
        code ]
      end
      ```
      ```ruby
      unless conditional [then]
        code
        [else                       #Ejecuta el código  si la variable conditional es falsa
          code ]                      
        end
        ```

30. ¿Cúal es la sintaxis para definir sentencias iterativas en Ruby?

    ```ruby
    while (condition) [do]
      statement
    end
    ```
    ```ruby
    until conditional [do]
      code                             #Ejecuta el código mientras la variable conditional sea falsa
    end
    ```  
    ```ruby
    for variable [, variable ...] in expression [do]
      code
    end
    ```


31. ¿Cúal es la sintaxis para definir expresiones regulares en Ruby?

    >`/pattern/`

32. ¿Cúal es el operador de emparejamiento (matching) en Ruby?:star:

  > El operador de emparejamiento es =~ .

33. ¿Qué es un bloque de código (Code blocks)? ¿Cúal es la sintaxis para definirlos en Ruby?

    >Es un trozo de código que puedes asociar con invocaciones de métodos, como si fuera un parámetro. Puedes usar los bloques de código para implementar retrollamadas, pasar bloques de código e implementar iteradores.

    >Un bloque de código están agrupados entre "{" y "}" o bien entre "do ... end".

34. ¿Qué es rvm - Ruby Version Manager?

    >Es una herramienta de línea de comandos que permite gestionar varias versiones de Ruby en un solo host, múltiples entornos de intérpretes y gemas.

35. ¿Cómo se obtiene la relación de intérpretes Ruby disponibles para ser instalados?

    >`rvm list known`

36. ¿Cómo se obtiene la relación de intérpretes Ruby ya instalados?

    >`rvm list`

37. ¿Cómo se establece el intérprete Ruby con el que se va ha trabajar?

    >`rvm use [version]`

38. ¿Cómo se sabe el intérprete Ruby con el que se está trabajando?

    >`ruby -v`

39. ¿Qué es pry?

    >Es una poderosa alternativa al intérprete de comandos IRB estándar para Ruby.

40. ¿Cómo se instala la gema pry?

    `gem install pry`
