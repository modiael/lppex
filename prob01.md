# Lenguajes y Paradigmas de Programación
## Hoja de Problemas
### Tema 1

<!-- Escuela Superior de Ingeniería Informática
Lenguajes y Sistemas Informáticos
Universidad de La Laguna -->

1. ¿Cuáles son los paradigmas de programación más importantes? Enumérelos y descríbalos.

    >Los paradigmas de programación más importantes son el imperativo y el declarativo.

    * El modelo imperativo describe la programación en términos del estado del programa y sentencias que cambian dicho estado. Los programas imperativos son un conjunto de instrucciones que le indican al computador cómo realizar una tarea( se centran en cómo debe implementarse, en cómo debería hacerlo el ordenador). Se incluyen:

        * ```Programación orientada a objetos```: Está basada en el imperativo, pero encapsula elementos denominados objetos.

        * ```Programación dinámica```: está definida como el proceso de dividir problemas en partes pequeñas.

        * ```Von Neumann```

    * En contraposición, el modelo declarativo está basado en el desarrollo de programas especificando o "declarando" un conjunto de propiedades y reglas que describen el problema y detallan su solución. La solución es obtenida mediante mecanismos internos de control, sin especificar exactamente cómo encontrarla (tan sólo se le indica a la computadora qué es lo que se desea obtener o qué es lo que se está buscando). Se incluyen:

        * ```Programación funcional```: emplean un modelo computacional basado en la definición recursiva de funciones (es de corte más matemático)

	    * ```Programación lógica```: basado en la definición de relaciones lógicas.

	    * ```Lenguajes de flujos de datos```: realizan su cálculo según el flujo de información entre nodos funcionales.

      * ```Basados en templates```


2. ¿Cuál es la diferencia entre el lenguaje máquina y el lenguaje ensamblador?

    >El lenguaje de máquina es el sistema de códigos directamente interpretable por un circuito microprogramable (el nivel más bajo), como el microprocesador de una computadora. Este lenguaje está compuesto por un conjunto de instrucciones en formato binario que determinan acciones a ser tomadas por la máquina.

    >No obstante, especificar programas a este nivel de detalle es una tarea enormemente tediosa y , a partir de esto, surgió el lenguaje ensamblador, con el objetivo de permitir expresar operaciones con abreviaciones mnemotécnicas. El lenguaje ensamblador es un lenguaje de programación de bajo nivel que consiste en un conjunto de mnemónicos que representan instrucciones básicas para los computadores. El lenguaje ensamblador da un paso y codifica esas instrucciones de ceros y unos en algo más entendible por el ser humano.

3. ¿En qué sentido son los lenguajes de programación de alto nivel mejores que el lenguaje ensamblador?

    >Son más fáciles de aprender, son lenguajes independientes de la arquitectura del ordenador, se acercan más al lenguaje humano natural y es menos tedioso realizar los programas en alto nivel, lo que implica mayor rapidez haciendo los programas.

4. ¿En qué circunstancias no tiene sentido programar en ensamblador?

    >Cuando el programador no tiene que preocuparse de la arquitectura interna del computador donde programa.

5. ¿Por qué hay tantos lenguajes de programación? :star:

    **EVOLUCION**
    >Se van poniendo nuevos nombres mientras evolucionan. Se va subiendo el nivel.
    ```
    Fortran                 Algol                   Smalltalk
    Cobol       ----->      Pascal      ----->      C++
    Basic                   Ada                     Eiffel
    ...                     ...                     ...
    GOTOS                   STRUCTURED              OOP
    ```
    **PROPOSITO ESPECÍFICO**
    >Muchos lenguajes fueron diseñados para un dominio de problemas específico.
    ```
    Lisp            Computación simbólica
    Prolog          Relaciones lógicas
    Snobol, Icon    Manipulación de texto
    Perl            Manipulación de texto y programación de sistemas
    ```
    **PREFERENCIAS PERSONALES**
    ```
    Brevedad de C
    Recursión versus iteración
    Punteros
    ```
6. ¿Qué hace un lenguaje de programación tenga éxito? :star:

    **EXPRESIVIDAD**
    >Cuanto más de propósito general sea mejor.
    ```
    Basic (primeras versiones) versus Common Lisp
    ```
    **FACILIDAD DE APRENDIZAJE**
    ```
    Basic
    Logo, Scratch,
    Pascal
    Java
    ```
    **FACIL DE IMPLEMENTAR**
    >Desde el punto de vista de los desarrolladores de compiladores e intérpretes.
    ```
    Basic
    Pascal
    ```
    **CÓDIGO ABIERTO**
    ```
    C
    ```
    **ESTANDARIZACIÓN**

    **CALIDAD DE LOS COMPILADORES**
    ```
    Fortran
    ```
    **RAZONES ECONÓMICAS, PATROCINIO, INERCIA**
    ```
    Cobol, PL/I    <--- IBM
    Ada            <--- Departamento de Defensa USA
    C#             <--- Microsoft
    Java           <--- Sun, Oracle, Google
    ```

7. Nombre dos lenguajes de cada una de las siguientes categorias: :star:
    * Von Neumann

        C, Ada, Fortran
    * Orientado a Objetos

        Ruby, Smalltalk, Java, Eiffel, C++, Scala
    * Concurrente

        Java, C#, Ada, Modula-3, Erlang, Elixir
    * Funcional

        Lisp, Scheme, Clojure, ML, Haskell, Scala
    * Lógico

        Prolog, ALF

8. ¿Qué distingue a los lenguajes declarativos de los lenguajes imperativos?

    >El enfoque de la **Programación Imperativa** consiste en describir cómo opera el programa (se le dice al ordenador cómo realizar una tarea).

    >La **Programación Declarativa** se enfoca en describir el resultado que se desea obtener sin escribir explícitamente los pasos algorítmicos a realizar.

9. ¿Qué organización se encarga de la diseminación y desarrollo del lenguaje de programación Ada? :star:

    >El Departamento de Defensa de los Estados Unidos.

10. ¿Cuál se considera el primer lenguaje de programación? :star:

    >Fortran.

11. ¿Cuál se considera el primer lenguaje funcional? :star:

    >FP

12. ¿Por qué los lenguajes concurrentes no se consideran una categoría aparte en la clasificiación de los lenguajes dada? :star:

    >Porque se consideran lenguajes de programación secuenciales extendidos con librerías, módulos y plugins que permiten alcanzar la programación concurrente.

13. Explique por qué se distingue entre compilación e interpretación.

    >Los intérpretes se distinguen de los compiladores, ya que los compiladores traducen un programa desde su descripción en lenguaje de alto nivel a código máquina; mientras que los intérpretes traducen y ejecutan a medida que sea necesario (instrucción por instrucción) y normalmente no guardan el resultado de dicha traducción.

14. ¿Cuáles son las ventajas y desventajas de la compilación frente a la interpretación?
    * **Interpretación**

      * ```Ventajas```: La interpretación permite mayor flexibilidad y mejores diagnósticos puesto que el código fuente se ejecuta directamente y el intérprete puede incluir un excelente depurador a nivel de código. Además, con la interpretación se pueden implementar características que no se podrían realizar con la interpretación, como por ejemplo traducir en el momento nuevo código que se genera mientras se ejecuta un programa.

      * ```Desventajas``` La desventaja principal de los interpretadores es que ,cuando se interpreta un programa, típicamente corre más lentamente que si hubiera sido compilado porque el interpretador debe analizar cada sentencia en el programa cada vez que es ejecutada y entonces realizar la acción deseada.

    * **Compilación**

      * ```Ventajas```: Los lenguajes compilados presentan mejor rendimiento y más eficiencia con respecto a los lenguajes interpretados puesto que una decisión hecha en tiempo de compilación es una decisión que no tiene que ser llevada a cabo en tiempo de ejecución. Además, los programas sólo se compilan una vez y ejecutada varias veces, por lo que hay un ahorro considerable en tiempo.

      * ```Desventajas```: La desventaja de los compiladores es que, dado que un compilador traduce el código fuente a un lenguaje máquina específico, los programas deben ser compilados específicamente para una arquitectura y sistema operativo en particular, mientras que los intérpretes implementan una máquina virtual cuyo "lenguaje máquina" es el lenguaje de programación de alto nivel. El intérprete lee las sentencias en ese lenguaje.

15. ¿Qué modelo sigue Java? ¿compilado, interpretado, ambos? ¿Cómo se puede saber?

    >Java es compilado e interpretado. Se puede saber porque el código fuente se compila y se pasa a un código intermedio denominado habitualmente “bytecode” , entendible por la máquina virtual Java. Después, es esta máquina virtual simulada, denominada Java Virtual Machine o JVM, la encargada de interpretar el bytecode dando lugar a la ejecución del programa (a código nativo del dispositivo final).Java no se ejecutan en nuestra máquina real (en nuestro ordenador o servidor) sino que Java simula una “máquina virtual” con su propio hardware y sistema operativo; se hizo independiente del hardware y del sistema operativo en que se ejecutaba.

16. ¿Qué modelo sigue Ruby? ¿compilado, interpretado, ambos? ¿Cómo se puede saber?

    >Ruby es un lenguaje interpretado.Se puede saber porque puede ser ejecutado sobre cualquier plataforma, siempre y cuando éste contenga un intérprete.

17. ¿Qué es un compilador?

    >Un compilador es un programa del sistema que se encarga de la traducción desde un lenguaje de alto nivel al lenguaje ensamblador o al lenguaje máquina. Es una herramienta para la programación de software que permite detectar los errores lógicos del código fuente de un programa.

18. ¿Qué es un intérprete?

    >Es un programa informático capaz de analizar y ejecutar otros programas. Los intérpretes sólo realizan la traducción a medida que sea necesaria, típicamente instrucción por instrucción, y normalmente no guardan el resultado de dicha traducción.

19. ¿Cuál es el comando para generar una pareja clave-privada clave-pública? ¿Con qué argumentos se llama?

    ```
    ssh-keygen
    ```
    -t para la seleccion del tipo. (Ejemplo: rsa)

    -b 2048 establece el número de bits en la clave a 2048. Por defecto es 1024.

    -f mykey establece el nombre del fichero de clave: mykey y mykey.pub. Si se omite, pregunta

    -N passphrase la passphrase a utilizar. Si se omite, pregunta

    -C comentario el comentario a añadir en la correspondiente línea de la clave pública. Si se omite es username@host

20. ¿Qué permisos debe tener la clave privada y dónde debe estar?

    >La clave privada debe tener permisos de lectura y escritura solamente por parte del propietario del fichero. Debe estar dentro del directorio oculto: ~/.ssh (fichero id_rsa)

21. ¿En qué directorio y fichero se publica una clave? ¿De qué máquina?

    >En el directorio ~/.ssh y en el fichero id_rsa.pub de la máquina en la que se esté trabajando.

22. ¿Cómo se llama el fichero en el que se guardan las máquinas conocidas por ssh?

    >El fichero known_hosts

23. ¿Cómo se llama y en qué lugar está el fichero en el que se especifican las opciones para el cliente ssh?

    >El fichero /etc/ssh/ssh_config o bien ~/.ssh/config

24. ¿Cuáles son las principales diferencias entre un sistema de control de cambios y configuraciones distribuído y uno centralizado?

    >Los repositorios en un sistema de control de versiones **centralizado** se encuentran únicamente en un servidor que contiene todos los archivos versionados, y varios clientes que descargan los archivos desde ese lugar central. Los clientes sólo reciben parte del repositorio, aquel con el que estén trabajando, para posteriormente subir los cambios al servidor. Sin embargo, esta configuración también tiene serias desventajas. La más obvia es el punto único de fallo que representa el servidor centralizado. Si ese servidor se cae durante una hora, entonces durante esa hora nadie puede colaborar o guardar cambios versionados de aquello en que están trabajando. Además, si el disco duro en el que se encuentra la base de datos central se corrompe, y no se han llevado copias de seguridad adecuadamente, pierdes absolutamente todo.

    >En un sistema de control de versiones **distribuido** los clientes no sólo descargan la última instantánea de los archivos: replican completamente el repositorio. Todos los clientes poseen el repositorio con todo su historial, por completo. Así, si un servidor muere, y estos sistemas estaban colaborando a través de él, cualquiera de los repositorios de los clientes puede copiarse en el servidor para restaurarlo. Esto favorece que se pueda trabajar offline y que se pueda hacer copia desde cualquier cliente.

25. ¿Qué es git? :star:

    >El control de versiones es el registro de los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que se puedan recuperar versiones específicas en un momento dado. **Git** es un software para control de versiones. Su principal característica es que es distribuido y permite el mantenimiento de versiones de aplicaciones cuando éstas tienen un gran número de archivos de código fuente de manera eficiente y fiable.

26. ¿Qué son github y bitbucket? :star:

    >Son plataformas de desarrollo colaborativo (servicios web) que permiten alojar repositorios (proyectos) usando un sistema de control de versiones. En el caso de Github sólo soporta Git. Bitbucket también soporta Mercurial.

27. ¿Qué es un repositorio? :star:

    >Es un sitio centralizado donde se almacena y mantiene información digital controlada por un sistema de control de versiones.

28. ¿Qué es una confirmación (commit)? **NO SEGURO**

    >Es una instantánea de cómo se encuentra el repositorio en un momento determinado del tiempo. Se guardan los ficheros, el comentario, metadatos como la fecha, usuario, SHA para chequear los archivos, etcétera.

    >Es un proceso por el cual los cambios realizados, una vez que ya están añadidos al área de preparación se introducen en el repositorio.

29. ¿Qué es una instantánea (stage o index)? **NO SEGURO**

    >El index es el lugar en git donde se almacena información ( se agregan ficheros) acerca de lo que va a ir en tu próxima confirmación.

    >Es una imagen de como esta el repositorio en un momento determinado.

30. ¿Qué es una etiqueta (tag)? :star:

    >Se trata de un sistema que tiene git para marcar un punto del historial de commits importante.

31. ¿Qué es una rama? **NO SEGURO**

    >Una rama git es un apuntador a una lista de confirmaciones adecuadamente enlazadas en git. Es realmente un simple archivo que contiene los 40 caracteres de una suma de control SHA-1, (representando la confirmación de cambios a la que apunta)

32. Complete los comandos: :star:

    ```
    $ git config --global user.name "Firstname Lastname"
    $ git config --global user.email "your_email@youremail.com"
    ```
33. ¿Cómo se pueden ver los valores de una configuración git?:star:

    >Usando `git config --list`.

34. ¿Cómo se puede hacer que git ignore ciertos ficheros?:star:

    >En git disponemos de un fichero llamado ```.gitignore``` . Todos aquellos ficheros que cumplan las reglas que aparezcan en ```.gitignore```, serán ignorados por git.

35. ¿Cómo se crea un repositorio git? :star:

    >Usando `git init`.

36. ¿Cómo se clona un repositorio git? :star:

    >Usando `git clone [url]`.

37. ¿Cómo se añaden cambios para que estén bajo el control de git? :star:

    >Usando `git add .`.

38. ¿Cómo se envían los cambios añadidos al repositorio? :star:

    >Usando `git commit`.

39. ¿Cómo se configura qué editor se ejecuta para los mensajes de registro (log)? :star:

    >Usando `git config --global core.editor [nombre]`.

40. ¿Para qué sirven y en qué difieren los ficheros /etc/gitconfig, ~/.gitconfig ~/.git/config?:star:

    `/etc/gitconfig` Configuracion git a nivel de todo el sistema. Si pasas la opción ```--system``` a git config, lee y escribe específicamente en este archivo.

    `~/.gitconfig` Configuracion git específico para el usuario.  Puedes hacer que Git lea y escriba específicamente en este archivo pasando la opción ```--global```.

    `~/.git/config` Configuracion git para un sólo repositorio.

    >Cada nivel sobrescribe los valores del nivel anterior, por lo que los valores de .git/config tienen preferencia sobre los de /etc/gitconfig.

41. ¿Cómo se ve el estado de mis ficheros con respecto al repositorio?:star:

    >Usando `git status`

42. ¿Qué significa que un fichero aparezca en la sección Changes to be committed: del estado de git?:star:

    >El fichero ha sido modificado y agregado al área de preparación, pero falta hacer la confirmación pertinente.

43. ¿Qué significa que un fichero aparezca en la sección new file: del estado de git ?:star:

    >Significa que el fichero está preparado para ser añadido al repositorio y este fichero es nuevo, por lo que no está bajo el control de versiones (el fichero no está ni en la última instantánea ni está en la área de preparación).


44. ¿Qué significa que un fichero aparezca en la sección modified:: del estado de git?:star:

    >Significa que un archivo bajo seguimiento ha sido modificado en el directorio de trabajo, pero no ha sido preparado todavía.

45. ¿Qué significa que un fichero esté sin seguimiento (untracked)?:star:

    >Significa que no está bajo el control de versiones; es decir, el fichero no está ni en la última instantánea ni está en la área de preparación).

46. ¿Qué compara el comando git diff cuando no se le pasan opciones? :star:

    >Este comando compara lo que hay en tu directorio de trabajo con lo que hay en tu área de preparación. El resultado te indica los cambios que has hecho y que todavía no has preparado.

    >**NOTA**: ```git diff –-staged``` si quieres ver los cambios que has preparado y que irán en tu próxima confirmación.

47. ¿Qué opción hay que pasarle a git commit para especificar el mensaje de log y evitar que abra el editor?:star:

    >La opción `-m` de la forma `git commit -m "Mensaje de log deseado"``

48. ¿Qué opción hay que pasarle a git commit para que añada todos los cambios efectuados en ficheros con seguimiento (tracked)?:star:

    >La opción `-a`. Esta opción hace que Git prepare todo archivo que estuviese en seguimiento antes de la confirmación, permitiendo saltarte el área de preparación (git add).

49. ¿Qué información muestra por la consola git log para cada asiento (commit) cuando se llama sin argumentos?:star:

    >Si no le pasas ningún argumento, `git log` lista las confirmaciones realizadas sobre ese repositorio en orden cronológico inverso. Es decir, las confirmaciones más recientes se muestran al principio. Este comando lista cada confirmación con su suma de comprobación SHA-1 (para verificar todo en Git antes de ser almacenado), el nombre y dirección de correo del autor, la fecha y el mensaje de confirmación.

50. ¿Qué hace la opción --graph de git log? :star:

    >Añade un pequeño gráfico ASCII mostrando el histórico de ramificaciones y uniones.


51. ¿Qué muestra el comando git remote?:star:

    >Muestra una lista con los nombres de los repositorios remotos que hayas configurado en el repositorio local git.

52. ¿Qué muestra el comando git remote -v?:star:

    >Además de mostrar el nombre con el que se conoce el repositorio remoto, muestra la URL asociada a ese repositorio remoto.

53. ¿Cómo se añade un nuevo repositorio remoto?:star:

    >Usando `git remote add [nombre_repositorio_remoto] [url]`

54. ¿Qué repositorio remoto queda establecido cuando se hace git clone? ¿Qué rama será seguida en el remoto por la rama master?:star:

    >`origin` es el nombre del repositorio remoto por defecto en git. La rama remota se llamará `origin/master`

55. ¿Cómo se envían los cambios en el repositorio local en la rama master al repositorio remoto apuntado por origin?:star:

    >Se envían con `git push (remoto) (rama)`.

56. ¿Qué diferencias hay entre git pull y git fetch?:star:

    >El comando `git fetch` recupera toda la información enviada al repositorio remoto desde la última vez que lo clonaste (o desde la última vez que ejecutaste este comando). Este comando sólo recupera la información y la pone en tu repositorio local (no la une automáticamente con tu trabajo ni modifica aquello en lo que estás trabajando). Hay que unir ambos manualmente a posteriori.

    > El comando `git pull`, por lo general, recupera la información del servidor del que clonaste, y automáticamente se intenta unir con el código con el que estás trabajando actualmente.Éste puede resultar un flujo de trabajo más sencillo y cómodo.

57. ¿Qué ocurre si se intenta enviar y mezclar los cambios actuales al repositorio remoto y alguien se ha adelantado y mezclado sus cambios primero? ¿Qué se debe hacer?

    >No dejará enviar y mezclar los cambios ya que se producirán conflictos entre ellos. Por lo tanto será necesario volver a traer desde el repositorio remoto el nuevo contenido mediante `git fetch` o `git pull`, y una vez preparado sin conflictos, enviar los cambios.

58. ¿Qué es una rama?**NO SEGURO**

    >Una rama git es un apuntador a una lista de confirmaciones adecuadamente enlazadas en git. Es realmente un simple archivo que contiene los 40 caracteres de una suma de control SHA-1, (representando la confirmación de cambios a la que apunta)

59. ¿Cuál es el propósito de trabajar con múltiples ramas?:star:

    >Tener múltiples versiones del repositorio que no interfieran unas con las otras para implementar distintas mejoras.

60. ¿Cómo se añade una nueva rama?:star:

    Usando `git branch [nueva_rama]`.

61. ¿Cómo se hace que una rama esté activa?:star:

    Usando `git checkout [rama]`.

62. ¿Cómo se listan el conjunto de ramas definidas?:star:

    Usando `git branch`.

63. ¿Qué es la fusión (merge) de ramas?:star:

    >Consiste en fusionar los cambios existentes de una rama en otra. Para ello, Git  determina automáticamente el mejor ancestro común de las dos ramas para realizar la fusión y después realiza una fusión a tres bandas, utilizando las dos instantáneas apuntadas por el extremo de cada una de las ramas y por el ancestro común a ambas dos.A continuación, crea una nueva instantánea resultante de la fusión a tres bandas y crea automáticamente una nueva confirmación de cambios  que apunta a ella.

    >De existir incompatibilidades, Git espera a realizar la confirmación cuando el usuario las arregle.

64. ¿Cómo se fusionan dos ramas?:star:

    >Se utiliza el comando `git merge <nombre-rama>` (Se fusiona la rama activa con la especificada (“nombre-rama”))

65. ¿Cómo se borra una rama?:star:

    Usando `git branch -d [rama]`.

66. ¿Cómo se recupera una rama de un repositorio remoto y se crea un rama local de seguimiento?**NO SEGURO**

    >Las ramas de seguimiento son ramas locales que tienen una relación directa con alguna rama remota. Para ello se utiliza el comando `git checkout -b [rama_local] [nombreremoto]/[rama]`. Así, tu rama local [rama_local] va a llevar (push) y traer (pull) hacia o desde [nombreremoto]/[rama].

67. ¿Qué es la reorganización (rebase) de ramas?:star:

    >Es un proceso de integración de ramas alternativo a la fusión (merge) que consiste en  coger todos los cambios confirmados en una rama y reaplicarlos sobre otra. El proceso consiste en que Git vaya al ancestro común de ambas ramas (donde estás actualmente y de donde quieres reorganizar), saque las diferencias introducidas por cada confirmación en la rama donde estás, guarde esas diferencias en archivos temporales, reinicie (reset) la rama actual hasta llevarla a la misma confirmación en la rama de donde quieres reorganizar, y, finalmente, vuelva a aplicar ordenadamente los cambios.

68. ¿Cuál es la ventaja de la reorganización?:star:

    > La ventaja de una rama reorganizada es que es tiene siempre un registro lineal: como si todo el trabajo se hubiera realizado en serie, aunque realmente se haya hecho en paralelo. Te permite guardar el historial de confirmaciones.

69. ¿Cuál es la principal diferencia entre fusión y reorganización?:star:

    >La principal diferencia entre ellas es el historial. La reorganización vuelve a aplicar cambios de una rama de trabajo sobre otra rama, en el mismo orden en que fueron introducidos en la primera. Mientras que la fusión combina entre sí los dos puntos finales de ambas ramas.


70. ¿Cuál es la principal desventaja de la reorganización? :star:

    >Se sobrescribe el historial del repositorio (se cambia la estructura de las confirmaciones) y, si estas trabajando de forma colaborativa en un repositorio compartido, se crean muchos problemas. Cuando reorganizas algo, estás abandonando las confirmaciones de cambio ya creadas y estás creando unas nuevas; que son similares, pero diferentes.

71. ¿Cuándo no se debe usar la reorganización?:star:

    >Nunca se deben reorganizar confirmaciones de cambio (commits) que hayas enviado (push) a un repositorio público.
